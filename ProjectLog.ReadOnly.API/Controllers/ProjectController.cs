﻿using DAL.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProjectLog.ReadOnly.API.Controllers
{
    [EnableCors(origins: "http://localhost:3000", headers:"*", methods:"GET")]
    public class ProjectController : ApiController
    {
        private IUnitOfWork uow;

        public ProjectController(IUnitOfWork unitOfWork)
        {
            uow = unitOfWork;
        }

        [HttpGet, Route("api/Project/{id}")]
        public HttpResponseMessage GetProjectExists(int id)
        {
            var project = uow.ProjectRepository.Get().Where(p => p.Id == id).FirstOrDefault();
            if (project == null)
                return Request.CreateResponse(HttpStatusCode.OK, false);
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }
    }
}
