# README #

This is an API to access data from within the Sales Project Log without the need for authentication. 

Due to the lack of authentication, this project is only to be used internally and is Read-Only.